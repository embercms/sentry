<?php

namespace Modules\SentryIntegration\Providers;

use App\Providers\ModuleServiceProvider;
use Closure;
use Psr\Http\Message\ServerRequestInterface;
use Sentry;
use Slim\Middleware\ErrorMiddleware;
use Throwable;

class SentryServiceProvider extends ModuleServiceProvider
{
    public function boot(): void
    {
        if ($this->app->environment() === 'development')
            return;

        $config = require $this->module->getPath('config.php');
        require $this->module->getPath('vendor/autoload.php');

        Sentry\init(['dsn' => $config['dsn']]);

        $closure = Closure::fromCallable([static::class, 'hookErrorMiddleware']);
        $this->callAfterResolving(ErrorMiddleware::class, $closure);
    }

    protected static function hookErrorMiddleware(ErrorMiddleware $errorMiddleware): void
    {
        $defaultErrorHandler = $errorMiddleware->getDefaultErrorHandler();

        $errorMiddleware->setDefaultErrorHandler(function (
            ServerRequestInterface $request,
            Throwable $exception,
            bool $displayErrorDetails,
            bool $logErrors,
            ...$params
        ) use ($defaultErrorHandler) {
            Sentry\captureException($exception);
            return $defaultErrorHandler($request, $exception, $displayErrorDetails, false, ...$params);
        });
    }
}
